from django.shortcuts import render

from rest_framework.viewsets import ModelViewSet
from django.views.generic import TemplateView
from iphone import models
from iphone.serializers import StudentSerializer

# Create your views here.

class StudentViewSet(ModelViewSet):
    queryset = models.Student.objects.all()
    serializer_class = StudentSerializer

